import React, { useState } from "react";
export default function NewTodo(props) {
  let [value, setValue] = useState("");
  const onChange = (e) => {
    e.preventDefault();
    setValue(e.target.value);
  };

  const onClick = (e) => {
    e.preventDefault();
    if (value.trim()) {
      props.onClick(value);
      setValue("");
    }
  };
  return (
    <form className="newTodo" onSubmit={onClick}>
      <input
        type="text"
        className="newTodo__input"
        value={value}
        onChange={onChange}
      ></input>
      <button type="submit" className="newTodo__button">
        &nbsp;+&nbsp;
      </button>
    </form>
  );
}
