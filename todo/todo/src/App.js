import React, { useState } from "react";
import TodoList from "./TodoList";
import NewTodo from "./NewTodo";

export default function App() {
  let [todos, setTodos] = useState([
    { id: 1, completed: false, content: "Прокатиться на велике" },
    { id: 2, completed: false, content: "Погулять по городу" },
    { id: 3, completed: false, content: "Съездить на море" },
  ]);
  function changeStatus(id) {
    setTodos(
      todos.map((todoElem) => {
        if (todoElem.id === id) todoElem.completed = !todoElem.completed;
        return todoElem;
      })
    );
  }

  function deleteItem(id) {
    setTodos(todos.filter((todoElem) => todoElem.id !== id));
  }

  function addTodo(content) {
    if (content.trim());
    setTodos(
      todos.concat([{ id: todos.length + 1, completed: false, content }])
    );
  }

  return (
    <div className="wrapper">
      <h3>Things to do:</h3>
      <NewTodo onClick={addTodo} />
      <TodoList
        todos={todos}
        changeStatus={changeStatus}
        deleteItem={deleteItem}
      />
    </div>
  );
}
