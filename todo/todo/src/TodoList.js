import React from "react";
import TodoItem from "./TodoItem";

const styles = {
  ul: {
    listStyle: "none",
    marginTop: "20px",
  },
};

export default function TodoList(props) {
  return (
    <ul style={styles.ul}>
      {props.todos.map((todoElem, index) => {
        return (
          <TodoItem
            key={todoElem.id}
            todoElem={todoElem}
            index={index}
            onChange={props.changeStatus}
            onClick={props.deleteItem}
          />
        );
      })}
    </ul>
  );
}
