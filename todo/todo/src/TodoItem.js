import React from "react";
import DeleteButton from "../src/remove.svg";

const styles = {
  li: {
    display: "flex",
    gap: "20px",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "5px",
    marginBottom: "10px",
    border: "1px solid lightgrey",
    borderRadius: "5px",
  },
  button: {
    border: "none",
    background: "transparent",
  },
};
export default function TodoItem(props) {
  let todoClasses = "todo";
  if (props.todoElem.completed === true) todoClasses += " completed";

  return (
    <li style={styles.li}>
      <span className={todoClasses}>
        <input
          type="checkbox"
          onChange={() => props.onChange(props.todoElem.id)}
        ></input>
        <strong>{props.index + 1}</strong>
        {props.todoElem.content}
      </span>
      <button
        style={styles.button}
        onClick={() => props.onClick(props.todoElem.id)}
      >
        <img className="deleteButtonImg" src={DeleteButton}></img>
      </button>
    </li>
  );
}
