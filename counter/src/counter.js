import React from 'react';
import {useState, useEffect} from 'react';

const styles = {
  button: {
    backgroundColor:'#0B4FBA',
    borderRadius: "10px",
    fontSize: '2rem',
    color: 'white',
    padding: '10px',
    border: 'none',

  }
}

export default function Counter() {
  const [count, setCount] = useState(0);
  useEffect(()=> {
    document.title = `Вы нажали кнопку ${count} раз`
  }
  );
  return (
    <>
    <button onClick={()=> setCount(count+1)} style={styles.button}>Вы нажали кнопку {count} раз(а)</button>
    </>
    
  )
}